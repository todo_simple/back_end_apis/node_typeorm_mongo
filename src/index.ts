import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import {Routes} from "./routes";

let port = 3001;
let TodoListRoutes = require('./routes/todoListRoute');


createConnection().then(async connection => {

    // create express app
    const app = express();
    app.use(bodyParser.json());// for parsing application/json
    app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
    

    // Configurar cabeceras y cors
    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
        res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
        res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
        next();
    });

    

    app.use('/getTodoList', TodoListRoutes);// Inicializa  las rutas  de todo list
    // setup express app here
    // ...

    // start express server
    app.listen(3001);

    // insert new item for test
    // await connection.manager.save(connection.manager.create(TodoSimple, {
    //     body: "Hola esto es una prueba",
    // }));


    console.log(`Express server has started on port 3000. Open http://localhost:${port} to see results`);

}).catch(error => {
    console.log(error)
});
