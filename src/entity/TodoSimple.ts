import { Entity, ObjectID, ObjectIdColumn, Column } from "typeorm";

@Entity()
export class TodoSimple {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    body: string;

    @Column()
    header: string;

    @Column()
    id_user: number;

}
