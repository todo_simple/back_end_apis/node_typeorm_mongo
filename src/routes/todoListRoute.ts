
// --- mirar pagina en  https://expressjs.com/es/4x/api.html#router 
// --- mirar pagina en  https://expressjs.com/es/4x/api.html#app.post.method
let express = require('express');
let router = express.Router(); // Utilice la clase express.Router para crear manejadores de rutas montables y modulares. Una instancia Router es un sistema de middleware y direccionamiento completo

import { holaMundo, all , save } from './../controller/TodoListController';
var multer = require('multer'); // v1.0.5
var upload = multer(); // for parsing multipart/form-data



// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});


// define the home page route
router.get('/', function (req, res) {
    res.send('Welcome to the TodoList home page');
});


/*todoList*/
router.route('/addNewItem').post(upload.array(),(req, res) => {
    save(req, res).then((resultado) => {
        res.send(resultado)
    });
});


router.get('/getAll', (req, res) => {
    all(req, res).then((resultado) => {
        res.send(resultado)
    });

});

router.get('/getGreeting', holaMundo);


module.exports = router;