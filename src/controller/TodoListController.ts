import { getConnection, getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { TodoSimple } from "../entity/TodoSimple";
import { log } from "util";




export function all(req: Request, res: Response) {
    const todoSimpleRepository = getRepository(TodoSimple);
    let list = todoSimpleRepository.find();
    return list;
}

export function save(req: Request, res: Response) {

    const todoSimpleRepository = getRepository(TodoSimple);
    const todoSimple = todoSimpleRepository.create(req.body);


    return todoSimpleRepository.save(todoSimple);
}

// export function one(request: Request, response: Response, next: NextFunction) {
//     return this.todoSimpleRepository.findOne(request.params.id);
// }

// export async function remove(request: Request, response: Response, next: NextFunction) {
//     let userToRemove = await this.todoSimpleRepository.findOne(request.params.id);
//     await this.todoSimpleRepository.remove(userToRemove);
// }

export function holaMundo(request: Request, response: Response, next: NextFunction) {
    response.send('Welcome to my uinchi');
}

